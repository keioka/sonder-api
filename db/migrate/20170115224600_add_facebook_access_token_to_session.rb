class AddFacebookAccessTokenToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :facebook_access_token, :string
  end
end
