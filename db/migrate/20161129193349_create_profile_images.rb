class CreateProfileImages < ActiveRecord::Migration
  def change
    create_table :profile_images do |t|
      t.references :profile, null: false
      t.references :image, null: false
      t.timestamps null: false
    end
  end
end
