class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.references :user
      t.references :post
      t.boolean :accepeted
      t.timestamps null: false
    end
  end
end
