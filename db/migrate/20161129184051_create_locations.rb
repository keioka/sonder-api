class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :city, null: false
      t.string :province, null: false
      t.string :country, null: false
      t.decimal :latitude, null: false
      t.decimal :longitude, null: false
      t.timestamps null: false
    end
  end
end
