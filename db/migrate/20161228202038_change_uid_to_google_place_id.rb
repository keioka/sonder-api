class ChangeUidToGooglePlaceId < ActiveRecord::Migration
  def change
    rename_column :locations, :uid, :google_place_uid
  end
end
