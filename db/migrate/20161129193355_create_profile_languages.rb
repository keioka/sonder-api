class CreateProfileLanguages < ActiveRecord::Migration
  def change
    create_table :profile_languages do |t|
      t.references :profile, null: false
      t.references :language, null: false
      t.timestamps null: false
    end
  end
end
