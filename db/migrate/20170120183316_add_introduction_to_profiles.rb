class AddIntroductionToProfiles < ActiveRecord::Migration
  def change
   add_column :profiles, :introduction, :string
  end
end
