class AddFacebookLikeUidToInterests < ActiveRecord::Migration
  def change
    add_column :interests, :facebook_like_uid, :string
  end
end
