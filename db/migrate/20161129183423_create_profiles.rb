class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.integer :gender, null: false
      t.date :birthday, null: false
      t.references :user, null: false
      t.timestamps null: false
    end
  end
end
