class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.string :session_token, null: false
      t.string :facebook_uid, null: false
      t.timestamps null: false
    end
  end
end
