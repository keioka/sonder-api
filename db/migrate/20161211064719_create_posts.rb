class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.datetime :datetime, null: false
      t.references :user, null: false
      t.references :location, null: false
      t.string :description, null: false
      t.timestamps null: false
    end
  end
end
