class AddApprovedToFriendShips < ActiveRecord::Migration
  def change
    add_column :friend_ships, :approved, :boolean, null: false, default: false
  end
end
