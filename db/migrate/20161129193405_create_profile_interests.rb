class CreateProfileInterests < ActiveRecord::Migration
  def change
    create_table :profile_interests do |t|
      t.references :profile, null: false
      t.references :interest, null: false
      t.timestamps null: false
    end
  end
end
