class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.references :session, null: false 
      t.references :location, null: false
      t.timestamps null: false
    end
  end
end
