require 'faker'
100.times do
  name = Faker::Name.name
  name_array = name.split(" ")

  params = {
    id: SecureRandom.hex,
    first_name: name_array[0],
    last_name: name_array[1],
    gender: Random.new.rand(0..1),
    birthday: Faker::Date.between(40.years.ago, 20.years.ago)
  }

  Session.create_or_find_by_facebook_uid(params)
end