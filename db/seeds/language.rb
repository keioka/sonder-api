all_languages = LanguageList::ALL_LANGUAGES

all_languages.each do |language|
  Language.create(name: language.name)
end
