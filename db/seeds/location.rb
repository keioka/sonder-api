@client = GooglePlaces::Client.new("AIzaSyDnd7VpVekR7d09azP_RQ5Bb_bQHKMkSVo")
google_location = @client.spots_by_query("San Francisco, CA", :types => ['cities'])[0]
format_address = google_location.formatted_address.split(",")
place_id = google_location.place_id

params = {
  city: format_address[0], 
  province: format_address[-2], 
  country: format_address[-1], 
  latitude: google_location.lat, 
  longitude: google_location.lng,
  google_place_uid: place_id
}

location = Location.create(params)