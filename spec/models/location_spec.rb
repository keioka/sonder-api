require 'rails_helper'

RSpec.describe Location, type: :model do
  describe "#find_or_create_by_api" do

    context "should create" do
      before do
        address = "San Francisco, CA, United States"
        @client = GooglePlaces::Client.new("AIzaSyA7ipuZM-rBKkEcX2XWwqmL72NMlbx_xfc")
        @google_location = @client.spots_by_query(address, :types => ['cities'])[0]
        @format_address = @google_location.formatted_address.split(",")
      end
     
      let(:location){Location.find_or_create_by_api("San Francisco, CA, United States")}
      
      it "should increment record count" do
        expect{ Location.find_or_create_by_api("San Francisco, CA, United States") }.to change(Location, :count).by(1)
      end

      it "should have city" do
        expect(location.city).to eql("San Francisco")
      end

      it "should have province" do
        expect(location.province).to eql("CA")
      end

      it "should have country" do
        expect(location.country).to eql("USA")
      end
      
      it "should have uid" do
        expect(location.google_place_uid).to eql(@google_location.place_id)
      end

      context "Location#find_or_create_by_string"
        it "should have uid" do
          expect(location.google_place_uid).to eql(@google_location.place_id)
        end

    end

    context "should find" do
      it "should return location" do
        
      end
    end
  end  
end
