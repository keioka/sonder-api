require 'rails_helper'

describe GooglePlaceParams do
  
  before do
    address = "San Francisco, CA, United States"
    @client = GooglePlaces::Client.new("AIzaSyA7ipuZM-rBKkEcX2XWwqmL72NMlbx_xfc")
    @google_location = @client.spots_by_query(address, :types => ['cities'])[0]
    @format_address = @google_location.formatted_address.split(",")
  end

  let(:google_place){GooglePlaceParams.new(address: "San Francisco, CA, United States", client: @client)}
  let(:google_place_invalid){GooglePlaceParams.new(address: "", client: @client)}

  context "#initialize" do
    it "should have attribute latitude" do
      expect(google_place.latitude).to eql(37.7749295)
    end

    it "should have attribute longitude" do
      expect(google_place.longitude).to eql(-122.4194155)
    end
    
    it "should have attribute country" do
      expect(google_place.country).to eql("USA")
    end

    it "should have attribute city" do
      expect(google_place.city).to eql("San Francisco")
    end

    it "should have attribute province" do
      expect(google_place.province).to eql("CA")
    end

    it "should have attribute google_place_uid" do
      expect(google_place.google_place_uid).to eql("ChIJIQBpAG2ahYAR_6128GcTUEo")
    end

    it "should raise error with invalid param" do
      expect{google_place_invalid}.to raise_error(ArgumentError)
    end
  end

  context "#to_hash_params" do
    it "should have attribute google_place_uid" do
      expect(google_place.to_hash_params[:city]).to eql("San Francisco")
    end
  end
end
