require 'rails_helper'

RSpec.describe Session, type: :model do
  
  it "has a valid factory" do
    profile = FactoryGirl.create(:profile)
    expect(profile).to be_valid
  end
  
  describe "Callback" do
    context "#before_create" do 
      params = { 
        id: "hghj",
        first_name: "Kei", 
        last_name: "Oka", 
        birthday: "08/28/1989", 
        gender: "male",
        location: "san francisco"
      }

      it ":generate_token should be created" do
        session = Session.create_account(params)
        expect(session.session_token.length).to eq(22)
      end
    end
  end

  params = { 
    id: "hghj",
    first_name: "Kei", 
    last_name: "Oka", 
    birthday: "08/28/1989", 
    gender: "male",
    location: "san francisco"
  }

  describe "Class Methods" do
    context "#create_account" do 
      it "sould be valid" do
        session = Session.create_account(params)
        expect(session).to be_valid
      end
    end
  end
end
