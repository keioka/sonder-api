require 'rails_helper'

RSpec.describe User, type: :model do
  
  context "Association" do

    before do 
      @user = FactoryGirl.create(:user)
      @user2 = FactoryGirl.create(:user, uid: "hdsai")
      @review = FactoryGirl.create(:review)
      @reviewing = FactoryGirl.create(:review, reviewee_id: 2, reviewer_id: 1)
    end

    it "should have reviews" do
      expect(@user.reviews.first).to eq(@review)
    end

    it "should have reviewing" do
      expect(@user.reviewings.first).to eq(@reviewing)
    end

  end

  context "#create" do

    before do 
      @session = FactoryGirl.create(:session)
      @user = FactoryGirl.create(:user)
      @review = FactoryGirl.create(:review)
    end

    it "should be valid" do
      expect(@user).to be_valid
    end
    
    context "should have valid uid" do
      it "length is 22" do
        uid_length = @user.uid.length
        expect(uid_length).to eq(22)
      end
     
      it "length is not 16" do
        uid_length = @user.uid.length
        expect(uid_length).not_to eq(16)
      end
    end

    context "Delegate" do
      it "should access to delegated facebook_uid" do
        expect(@user.facebook_uid).to eq(@session.facebook_uid)
      end
    end

  end
end
