require 'rails_helper'

RSpec.describe Profile, type: :model do
  describe "Class Methods" do
    context "#create" do 
      before do 
        @profile = FactoryGirl.create(:profile)
      end
      it "sould be valid" do
        @profile = FactoryGirl.create(:profile)
        expect(@profile).to be_valid
      end

      it "should have introduction" do
        @profile = FactoryGirl.create(:profile)
        expect(@profile.introduction).to eql("Hello")
      end
    end
  end

end
