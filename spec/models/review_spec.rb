require 'rails_helper'

RSpec.describe Review, type: :model do
  context "#create" do
    context "with valid params" do
      before do
        @reviewer = FactoryGirl.create(:user)
        @reviewee = FactoryGirl.create(:user)
      end

      let(:review){ FactoryGirl.create(:review)}

      it "should have reviewer_id" do
        expect(review.reviewer_id).to eq(2)
      end

      it "should have reviewer user" do
        expect(review.reviewer).to be_a(User)
      end

      it "should have reviewee user" do
        expect(review.reviewer).to be_a(User)
      end
    end

    # let blocks
    # Code within a let block is only executed when referenced, lazy loading this means that ordering of these blocks is irrelevant. This gives you a large amount of power to cut down on repeated setup through your specs.

    context "with invalid params" do
      let(:review){ FactoryGirl.create(:review, rating: 7)}
      it "should have reviewer_id" do
        expect { review }.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end
end
