require 'rails_helper'

RSpec.describe Post, type: :model do

  before do 
    @post = FactoryGirl.create(:post)
  end

  it "should be valid" do
    expect(@post).to be_valid
  end
  
  context "should have valid uid" do
    it "length is 32" do
      uid_length = @post.uid.length
      expect(uid_length).to eq(32)
    end
   
    it "length is not 16" do
      uid_length = @post.uid.length
      expect(uid_length).not_to eq(16)
    end
  end

end
