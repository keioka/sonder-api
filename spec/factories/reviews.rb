FactoryGirl.define do
  factory :review do
    reviewee_id 1
    reviewer_id 2
    content "MyString"
    rating 1
  end
end
