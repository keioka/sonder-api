FactoryGirl.define do
  factory :user do
    session_id 1
    location_id 1
    uid SecureRandom.urlsafe_base64(nil, false)
  end
end
