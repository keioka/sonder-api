FactoryGirl.define do
  factory :location do
    google_place_uid "7873j2u282k9"
    city "San Francisco"
    province "California"
    country "U.S"
    latitude 123.3243434
    longitude 143.32223   
  end
end
