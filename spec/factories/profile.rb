FactoryGirl.define do
  factory :profile do
    first_name { Faker::Name.name }
    last_name { Faker::Name.name }
    gender 1
    birthday { Faker::Date.between(20.year.ago, 40.year.ago) }
    introduction "Hello"
    user_id 1
  end
end
