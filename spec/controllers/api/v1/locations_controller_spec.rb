require 'rails_helper'

RSpec.describe Api::V1::LocationsController, type: :controller do
  before do 
    @location = FactoryGirl.create(:location) 
    @user = FactoryGirl.create(:user) 
    @profile = FactoryGirl.create(:profile)
    @post = FactoryGirl.create(:post) 
  end
 
  describe "#show" do

    it "should return 200" do
      get :show, id: 1
      expect(response).to be_success
    end

    it "should return city `San Francisco`" do
      get :show, id: 1
      parsed_response = JSON.parse(response.body)
      expect(parsed_response["city"]).to eql(@location.city)
      expect(parsed_response["province"]).to eql(@location.province)
      expect(parsed_response["country"]).to eql(@location.country)
    end
    
    it "should return users" do
      get :show, id: 1
      parsed_response = JSON.parse(response.body)
      expect(parsed_response["users"].length).to be(1)
    end
    
    it "should return users profile" do
      get :show, id: 1
      parsed_response = JSON.parse(response.body)
      users = parsed_response["users"]
      user = users[0]
      profile = user["profile"]
      expect(parsed_response["profile"]["first_name"]).to be(@profile.first_name)
    end

    it "should return posts" do
      get :show, id: 1
      parsed_response = JSON.parse(response.body)
      expect(parsed_response["posts"].length).to be(1)
    end

  end
end
