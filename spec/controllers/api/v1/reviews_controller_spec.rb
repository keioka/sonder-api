require 'rails_helper'

RSpec.describe Api::V1::ReviewsController, type: :controller do

  describe "POST #create" do
    it "returns http success" do
      post :create, id: 1
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #index" do
    it "returns http success" do
      get :index, id: 1
      expect(response).to have_http_status(:success)
    end
  end

end
