require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
    before do 
      @session = FactoryGirl.create(:session)
      @location = FactoryGirl.create(:location) 
      @user = FactoryGirl.create(:user) 
      @profile = FactoryGirl.create(:profile)
      @post = FactoryGirl.create(:post) 
      @params = {
        "body": {
          id: @session.facebook_uid,
          first_name: "Kei", 
          last_name: "Oka", 
          birthday: "08/28/1989", 
          gender: "male",
          location: "san francisco",
          accessToken: "dadwe211_221"
        }
      }
      @headers = {'Accept' => 'application/json', 'Content-Type' => 'application/json'}
    end

    #
    # auth
    #

    context "#auth" do
      context "find exsisted user" do
        it "should return 200" do
          post :auth, @params, @headers
          expect(response).to be_success
        end

        it "should return first_name" do
          post :auth, @params, @headers
          response_body = JSON.parse(response.body)
          expect(response_body["current_user"]["profile"]["first_name"]).to eql("Kei")
        end
      end

      context "should create user" do
        before do 
          @params = {
            "body": {
              id: @session.facebook_uid,
              first_name: "Kei", 
              last_name: "Oka", 
              birthday: "08/28/1989", 
              gender: "male",
              location: "san francisco",
              accessToken: "dadwe211_221"
            }
          }
          headers = {'Accept' => 'application/json', 'Content-Type' => 'application/json'}
        end

        it "should return 200" do
          post :auth, @params, @headers
          expect(response).to be_success
        end

        it "should have first_name" do
          post :auth, @params, @headers
          response_body = JSON.parse(response.body)
          expect(response_body["current_user"]["profile"]["first_name"]).to eql(@params[:body][:first_name])
        end

        it "should have last_name" do
          post :auth, @params, @headers
          response_body = JSON.parse(response.body)
          expect(response_body["current_user"]["profile"]["last_name"]).to eql(@params[:body][:last_name])
        end
  
        it "should have location" do
          post :auth, @params, @headers
          response_body = JSON.parse(response.body)
          expect(response_body["current_user"]["location"]["city"]).to eql("San Jose")
        end
      end
    end 


    #
    #  current
    #
    
    context "#current" do
      before do 
        @params = {"body": {"token": @session.session_token}}
        headers = {'Accept' => 'application/json', 'Content-Type' => 'application/json'}
      end

      it "should return 200" do
        post :current, @params, @headers
        expect(response).to be_success
      end

      it "should return different session_token" do
        post :current, @params, @headers
        body = JSON.parse(response.body)
        expect(body["session_token"]).not_to eq(@session.session_token)
      end 
    end


    #
    # update_profile
    #

    context "#update_profile" do
      before do 
        @params = { "body": { 
          "token": @session.session_token, 
            "profile": { 
            "introduction": "Hello",
              "birthday": Time.now 
            }, 
            "location": "San Mateo, CA, US", 
            "languages": ['Japanese'], 
            "interests":['Snowboarding']
          }
        }
        @headers = {'Accept' => 'application/json', 'Content-Type' => 'application/json'}
      end
      
      it "should return 200 do profile_update" do
        post :update_profile, @params, @headers
        expect(response).to be_success
      end

      it "should have profile introduction" do
        post :update_profile, @params, @headers
        body = JSON.parse(response.body)
        expect(body["current_user"]["profile"]["introduction"]).to eq("Hello")
      end

      it "should return location" do
        post :update_profile, @params, @headers
        body = JSON.parse(response.body)
        expect(body["current_user"]["location"]["city"]).to eq("San Mateo")
      end

      it "should return languages" do
        post :update_profile, @params, @headers
        body = JSON.parse(response.body)
        expect(body["current_user"]["profile"]["languages"][0]["name"]).to eq("Japanese")
      end
     
      it "should return interests" do
        post :update_profile, @params, @headers
        body = JSON.parse(response.body)
        expect(body["current_user"]["profile"]["interests"][0]["name"]).to eq("Snowboarding")
      end

    end
end
