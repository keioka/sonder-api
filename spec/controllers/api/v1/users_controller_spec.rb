require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  before do 
    @session = FactoryGirl.create(:session)
    @location = FactoryGirl.create(:location) 
    @user = FactoryGirl.create(:user, uid: "abcde") 
    @user2 = FactoryGirl.create(:user, uid: "abcdefg") 
    @profile = FactoryGirl.create(:profile)
    @review = FactoryGirl.create(:review)
    @post = FactoryGirl.create(:post) 
    @params = {"body": {"id": @session.facebook_uid}}
    @headers = {'Accept' => 'application/json', 'Content-Type' => 'application/json'}
  end

  context "#show" do
    it "should return 200" do
      get :show, id: "abcde"
      expect(response).to be_success
    end

    context "attribute reviews" do
      it "should have reviews" do
        get :show, id: "abcde"
        response_body = JSON.parse(response.body)
        expect(response_body["reviews"].length).to eq(1)
      end

      it "should have reviewer" do
        get :show, id: "abcde"
        response_body = JSON.parse(response.body)
        expect(response_body["reviews"].first).to eq(1)
      end
    end
  end

  context "#location" do
    it "should return 200" do
      get :locations
      expect(response).to be_success
    end
    
    it "should have 1 record" do
      get :locations
      response_body = JSON.parse(response.body)
      expect(response_body.length).to eq(1)
    end

  end
end
