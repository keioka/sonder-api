require 'rails_helper'

RSpec.describe Api::V1::PostsController, type: :controller do
  context "#Show" do
    before do 
      @location = FactoryGirl.create(:location) 
      @user = FactoryGirl.create(:user) 
      @profile = FactoryGirl.create(:profile)
      @post = FactoryGirl.create(:post) 
    end
 
#  xit "should return 200" do
#    get :show, id: 1
#    expect(response).to be_success
#  end
# 
#  xit "Content-type should be JSON" do
#    expect(response.header['Content-Type']).to be 'application/json'
#  end
#  
#  xit "should have location" do 
#    get :show, id: 1
#    parsed_response = JSON.parse(response.body)
#    expect(parsed_response["location"]["google_place_uid"]).to eql(@location.google_place_uid)
#  end
# 
#  context "should have user"
#    xit "uid" do 
#      get :show, id: 1
#      parsed_response = JSON.parse(response.body)
#      expect(parsed_response["by_user"]["uid"]).to eql(@user.uid)
#    end
# 
#    xit "profile => first_name" do
#      get :show, id: 1
#      parsed_response = JSON.parse(response.body)
#      expect(parsed_response["by_user"]["profile"]["first_name"]).to eql(@profile.first_name)
#    end
# 
#    xit "profile => last_name" do
#      get :show, id: 1
#      parsed_response = JSON.parse(response.body)
#      expect(parsed_response["by_user"]["profile"]["last_name"]).to eql(@profile.last_name)
#    end
#   
#    xit "should have location_id" do
#      get :show, id: 1
#      parsed_response = JSON.parse(response.body)
#      p parsed_response
#      expect(parsed_response["by_user"]["location"]["city"]).to eql("San Francisco")
#    end
#  
#    xit "should have location_id" do
#      get :show, id: 1
#      parsed_response = JSON.parse(response.body)
#      expect(parsed_response["id"]).to be(1)
#    end
#   
#    xit "should have location_id" do
#      get :show, id: 1
#      parsed_response = JSON.parse(response.body)
#      expect(parsed_response["id"]).to be(1)
#    end
#   
#    xit "should return 200" do
#      get :show, id: 1
#      expect(response).to be_success
#    end

#    xit "should have location_id" do
#    end
  end

  context "#create" do
    before do
      @user = FactoryGirl.create(:user)
      @params = {
        "body": {
          datetime: "2017-01-04 16:25",
          description: "hi", 
          user_id: 1, 
          location: {
            google_place_uid: "dsfd",
            city: "Okayama", 
            province: "Okayama", 
            country: "Japan", 
            latitude: 32.4232,
            longitude: 45.244242 
          }
        }
      }

      @headers = {'Accept' => 'application/json', 'Content-Type' => 'application/json'}
    end
    it "should return 200" do
      post :create, @params, @headers
      expect(response).to be_success
    end
 
    it "should have description" do
      post :create, @params, @headers
      response_body = JSON.parse(response.body)
      expect(response_body["description"]).to eq("hi")
    end
   
    it "should have location" do
      post :create, @params, @headers
      response_body = JSON.parse(response.body)
      expect(response_body["location"]["google_place_uid"]).to eq("dsfd")
    end


  end
end
