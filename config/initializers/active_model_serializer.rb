ActiveModel::Serializer.config do |config|
  config.default_includes = '**'
  config.adapter = :json_api
end
