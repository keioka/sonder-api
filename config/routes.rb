Rails.application.routes.draw do

  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :sessions, only: [:auth, :current, :update_profile] do
        collection do 
          post 'auth' => 'sessions#auth'
          post 'current' => 'sessions#current'
          post 'update_profile' => 'sessions#update_profile'
      
        end
      end

      resources :locations, only: [:show, :users] do
        member do
          get 'posts' => 'posts#location'
        end
      end
    
      resources :users, only: [:index, :show, :update] do
        collection do
          get 'locations' => 'users#locations'
        end

        member do
          get 'reviews' => 'reviews#index'
          post 'reviews' => 'reviews#create'
        end

        resources :friends, only: [:request, :approve] do
          member do
            post 'request' => 'friends#create'
            post 'approve' => 'friends#approve'
          end
        end
      end
      
      resources :messages, only: [:retrive, :send] do
        collection do
          get 'retrive' => 'message#retrive'
          post 'send' => 'messages#send'
        end 
      end
    
      resources :posts do
        member do 
          post 'request' => 'posts#post_request'
          post 'approve' => 'posts#approve'
        end
      end
    end
  end
  # root 'welcome#index'

  # get 'products/:id' => 'catalog#view'

  # get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
