require 'singleton'

class GooglePlaceClient
  include Singleton

  attr_reader :google_place_client

  def initialize
    @google_place_client = GooglePlaces::Client.new("AIzaSyAA0YFo2s5wGwkionQFP-9k4cJMfS0hBGM")
  end
end

#=> http://www.rubydoc.info/stdlib/singleton/Singleton
#=> http://stackoverflow.com/questions/10733717/initializing-ruby-singleton

# private method `new' called for GooglePlaceClient:Class
#=> http://stackoverflow.com/questions/26700691/why-am-i-getting-private-method-new-error-only-inside-a-class-method-definitio