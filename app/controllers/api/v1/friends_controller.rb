class Api::V1::FriendsController < ApplicationController
  def create
    user_id = params[:user_id]
    user = User.find(user_id)
    if user 
      friend = User.find(params[:id])
      user.friends << friend
    else
      render json: {status: 404, message: "User is not found"}
    end
  end

  def approve
    user_id = params[:id]
    friend_id = 2
    user = User.find(user_id)
    friend = user.reverse_friends.find{|friend| friend.id == friend_id }
    friend.approve
  end
end
