class Api::V1::UsersController < ApplicationController
 
  def index
    users = User.all
    render json: users, each_serializer: UserPlainSerializer, status: 200
  end

  def show
    uid = params[:id]
    user = User.find_by_uid(uid)
    if user 
      render json: UserShowSerializer.new(user)
    else 
      render json: { message: "Not Found", status: 404 }, status: 404
    end   
  end

  def locations
    locations = User.all.map(&:location).uniq(&:id)
    render json: locations, each_serializer: LocationPlainSerializer
  end

end
