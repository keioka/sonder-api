class Api::V1::SessionsController < ApplicationController
  
  def auth
    session = Session.create_or_find_by_facebook_uid(params_body)
    if session
      render json: SessionSerializer.new(session), status: 200
    else
      render json: {message: "Auth is failed"}, status: 400
    end
  end

  def current
    token = params_body[:token]
    session = Session.find_by(session_token: token)
    if session
      session.update_token
      render json: session, status: 200
    else
      render json: { error: "Session is not found. Please check authorization." }, status: 404
    end
  end

  def update_profile
    token = params_body[:token]
    session = Session.find_by(session_token: token)
    if session
      session.user.update_user(profile_params)
      render json: SessionSerializer.new(session), status: 200
    else
      render json: { error: "Session is not found. Please check authorization." }, status: 404
    end
  end

  private
  def profile_params
    ActionController::Parameters.new(params_body).permit!
  end 
  
end
