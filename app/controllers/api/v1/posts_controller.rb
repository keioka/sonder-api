class Api::V1::PostsController < ApplicationController

  def index
    @posts = Post.all
    render json: @posts, each_serializer: PostShowSerializer, status: 200

  end

  def show
    @post = Post.find_by(uid: params[:id]) 
    if @post
      render json: @post, serializer: PostShowSerializer, status: 200, content_type: 'application/json'
    else
      render json: { message: "Not found", status: 404 }, status: 404
    end
  end

  def create
    location = Location.find_or_create_by(location_params)

    params = {
      datetime: params_body[:datetime],
      description: params_body[:description],
      user_id: params_body[:user_id],
      location_id: location.id
    }

    post = Post.create(params)
    render json: post, serializer: PostShowSerializer, status: 200, content_type: 'application/json'
  end

  def post_request
  end

  private
    def location_params
      ActionController::Parameters.new(params_body[:location]).permit(:google_place_uid, :province, :city, :country, :latitude, :longitude )

    end

end
