class Api::V1::LocationsController < ApplicationController
  def show
    id = params[:id]
    location = Location.includes(:users, :posts).find(id)
    render json: LocationShowSerializer.new(location)
  end

  def users
    id = params[:id]
    users = User.find_by_city_id(id)
    render json: users
  end

  def posts
    id = params[:id]
    posts = Post.find_by_location_id(id)
    render json: posts
  end

end
