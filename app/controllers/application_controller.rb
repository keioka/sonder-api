class ApplicationController < ActionController::API
 # include ActionController::RequestForgeryProtection
  
  #protect_from_forgery
  rescue_from ActiveRecord::StatementInvalid, with: :error_handle_400
  rescue_from ActiveModel::ForbiddenAttributesError, with: :error_handle_403
  rescue_from ActionController::RoutingError, with: :error_handle_404
  rescue_from ActiveRecord::RecordNotUnique, with: :not_unique_record

  def parsed_params
    params["body"].is_a?(String) ? JSON.parse(params["body"]) : params["body"]
  end

  def params_body
    parsed_params.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
  end

  def error_handle_400(exception = nil)
    message = exception.message || "NO_ERROR_MESSAGE"
    logger.warn "Rendering 400 with exception: #{exception.message}" if exception
    render json: { error: "400 Bad Request", message: message }, status: 400
  end

  def error_handle_403(exception = nil)
    message = exception.message || "NO_ERROR_MESSAGE"
    logger.warn "Rendering 403 with exception: #{exception.message}" if exception
    render json: { error: "403 Forbidden", message: message }, status: 403
  end

  def error_handle_404(exception = nil)
    message = exception.message || "NO_ERROR_MESSAGE"
    logger.warn "Rendering 404 with exception: #{exception.message}" if exception
    render json: { error: "404 Not Found", message: message }, status: 404
  end

  def error_handle_500(exception = nil)
    message = exception.message || "NO_ERROR_MESSAGE"
    logger.warn "Rendering 500 with exception: #{exception.message}" if exception
    render json: { error: "500 Not Found", message: message }, status: 500
  end

  def not_unique_record(exception = nil)
    logger.warn(exception)
  end
end
