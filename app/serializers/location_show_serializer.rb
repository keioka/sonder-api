class LocationShowSerializer < LocationBaseSerializer
  attributes :google_place_uid, :city, :province, :country, :latitude, :longitude, :users, :posts  
end
