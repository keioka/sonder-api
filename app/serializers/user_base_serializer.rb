class UserBaseSerializer < ApplicationSerializer
 
  def profile
    return {} unless object.profile
    profile = ProfileSerializer.new(object.profile)
    profile
  end

  def location
    return {} unless object.location
    location = LocationPlainSerializer.new(object.location)
    location
  end

  def posts
    return [] unless object.posts
    if scope && scope[:current_user]
      options = scope
      posts = map(object.posts, ::PostShowSerializer, options)
    else
      posts = map(object.posts, ::PostShowSerializer) 
    end
    posts
  end

  def reviews
    reviews = map(object.reviews, ReviewBaseSerializer)
    reviews
  end

  def friends
    result = []
    object.all_friends.each do |friend|
      user = FriendSerializer.new(friend)
      result << user
    end
    result
  end

  def friend_requested_users
    request = []
    if scope && scope[:session_user]
      request = object.friend_requested_users
    end
    request
  end 

end
