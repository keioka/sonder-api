class FriendSerializer < ApplicationSerializer
  attributes :id, :profile, :location
  
  def profile
    profile = object.profile ? ProfileSerializer.new(object.profile) : nil
    profile
  end

  def location
    location = LocationSerializer.new(object.location)
    location
  end
end
