class PostShowSerializer < PostBaseSerializer
  attributes :uid, :datetime, :description, :location, :by_user
  has_many :requests, :if => :current_user?
  
  def current_user?
    scope && scope[:current_user]
  end
end
