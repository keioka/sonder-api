class PostBaseSerializer < ApplicationSerializer

  def location
    location = ::LocationPlainSerializer.new(object.location)
    location
  end

  def by_user
    user = ::UserPlainSerializer.new(object.user)
    user
  end
end
