class ReviewBaseSerializer < ApplicationSerializer
  attributes :content, :rating, :created_at, :reviewer

  def reviewer
    UserPlainSerializer.new(object.reviewer)
  end
end
