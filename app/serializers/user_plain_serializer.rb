class UserPlainSerializer < UserBaseSerializer
  attributes :uid, :profile, :location
end
