class UserShowSerializer < UserBaseSerializer
  attributes :uid, :id, :profile, :location, :posts , :reviews
end
