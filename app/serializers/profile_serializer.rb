class ProfileSerializer < ApplicationSerializer
  attributes :first_name, :last_name, :gender, :birthday, :introduction, :created_at, :languages, :interests, :images

  def languages
    result = object.languages ?
 map(object.languages, LanguageSerializer) : nil
    result
  end

  def images
    results = object.images ? map(object.images, ImageSerializer) : nil
    results
  end
end
