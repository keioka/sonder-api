class PostPlainSerializer < ActiveModel::Serializer
  attributes :uid, :datetime, :description
end
