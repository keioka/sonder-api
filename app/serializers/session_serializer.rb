class SessionSerializer < ApplicationSerializer
  attributes :id, :session_token, :current_user
  
  def id
    object.user.id
  end

  def current_user
    options = { current_user: true }
    user = ::UserShowSerializer.new(object.user, options)
    user
  end
end
