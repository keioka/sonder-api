class LocationBaseSerializer < ApplicationSerializer
  
  def users
    users = map(object.users, ::UserShowSerializer)
    users
  end

  def posts
    posts = map(object.posts, ::PostShowSerializer)
    posts
  end

end
