class ApplicationSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  
  def map(object_array, serializer, options={})
    result = object_array.map do |object|
      serializer.new(object, options)
    end
    result
  end

end

# Can not filter attributes dymically?
# https://github.com/rails-api/active_model_serializers/issues/1245
