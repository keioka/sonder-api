require 'koala'

class FacebookGraphApi
  attr_reader :client

  def initialize(access_token)
    @client = Koala::Facebook::API.new(access_token)
  end

  def get_user_photo
    @client.get_object("me?fields=picture.width(9999).height(9999)")
  end

  def get_user_likes
    @client.get_connections("me", "likes")
  end
end