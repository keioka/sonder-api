class Signup

  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include DateHelper

  @@PROFILE_ATTRIBUTES = [:user_id, :first_name, :last_name, :gender, :birthday]

  PROFILE_IMAGE_ATTRIBUTES = [:picture]
  PROFILE_LANGUAGE_ATTRIBUTES = [:languages]

  def initialize(params)
    @params = params
  end

  def save
    Session.transaction do
      @session = Session.create(facebook_uid: facebook_uid)

      User.transaction(requires_new: true) do
        location = Location.find_or_create_by_api(location_params)
        user = User.create(session_id: @session.id, location_id: location.id)

        Profile.transaction(requires_new: true) do
          Profile.create_with_options(profile_params.merge!({user_id: user.id}), facebook_access_token)
        end

      end

    end
    @session
  end

  private

  def facebook_access_token
    @params[:accessToken]
  end

  def facebook_uid
    @params[:id]
  end

  def location_params
    @params[:location]
  end

  def profile_params
    profile_params = @params.select{|key, value| @@PROFILE_ATTRIBUTES.include?(key)}.inject({}) do |memo, (k, v)| 
      v = parseFbDateFormat(v) if k.to_sym == :birthday
      memo[k.to_sym] = v 
      memo
    end

    profile_params
  end

  def image_params
    @params.select{|key, value| PROFILE_IMAGE_ATTRIBUTES.include?(key)}
  end


end