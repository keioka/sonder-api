class ProfileGenerator

  def initialize(profile, profile_params, access_token=nil)
    @profile = profile
    @params = profile_params
    @api = access_token ? FacebookGraphApi.new(access_token) : nil
  end

  def init_sub_profile
    create_profile_photo
    create_profile_likes
  end

  def update_sub_profile
    update_profile_languages
    update_profile_likes
  end

  private

  def create_profile_photo
    raise "Facebook Graph API is invalid" unless api_available?
    user_photo = @api.get_user_photo

    if user_photo
      if user_photo.is_a?(Array)
        @profile.images << user_photo.map{|photo| Image.create(url: photo.url)}
      else
        url = user_photo["picture"]["data"]["url"]
        if url
          @profile.images << Image.create(url: url)
        end
      end
    end

    self
  end

  def create_profile_likes
    raise "Facebook Graph API is invalid" unless api_available?
    #get like from facebook graoh api and create tag
    likes = @api.get_user_likes
    interests_from_likes = likes.map do |like| 
      name = like["name"]
      facebook_like_uid = like["id"]
      Interest.find_or_create_by(name: name, facebook_like_uid: facebook_like_uid)
    end

    @profile.interests << interests_from_likes

    self
  end

  def update_profile_likes
    interests = @params[:interests]
    if interests && interests.instance_of?(Array)
      begin
         @profile.interests = interests.map!{|interest| Interest.find_or_create_by(name: interest)}
      rescue => e
      end
    end
    self
  end

  def update_profile_languages
    languages = @params[:languages]
    if languages && languages.instance_of?(Array)
      begin
        @profile.languages = languages.map!{|language| Language.find_or_create_by(name: language)}
      rescue => e
      end
    end
    self
  end

  private
    def api_available?
      !@api.nil?
    end

end