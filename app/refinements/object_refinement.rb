module ObjectRefinement
  refine Object do

    include SymbolRefinement
    using SymbolRefinement

    # to_hash_params
    # Create hash based paramter from readable attributes

    def to_hash_params
      Hash[instance_variables_readable.map { |name| [name.to_s.gsub("@", "").to_sym, instance_variable_get(name)] } ]
    end

    private
      def instance_variables_readable
        not_variables = extract_variable_name - own_instance_methods
        instance_variables_readable = extract_variable_name - not_variables
        instance_variables_readable.map{|variable| variable.instace_variable_symbol}
      end

      def own_instance_methods
        (self.class.instance_methods - Object.instance_methods)
      end

      def extract_variable_name
        instance_variables.map{|variable| variable.extract_instace_variable_name}
      end

  end
end