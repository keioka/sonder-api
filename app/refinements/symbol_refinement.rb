module SymbolRefinement
  refine Symbol do
    def instace_variable_symbol
      ("@" + self.to_s).to_sym
    end

    def extract_instace_variable_name
       self.to_s.gsub(/@/, "").to_sym
    end
  end
end