module ParamsObject
  using ObjectRefinement
  def to_hash_params
    super
  end

  alias :get :to_hash_params 
end