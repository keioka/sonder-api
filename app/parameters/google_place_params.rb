class GooglePlaceParams
  include ParamsObject

  attr_reader :google_place_uid, :city, :province, :country, :latitude, :longitude
  # initialize(google_place_client)
  #   @params: GooglePlaceClient take GooglePlaceClient to put test version of api client.

  def initialize(args)
    @google_place_client = args[:client] || GooglePlaceClient.instance.google_place_client
    invoke_location_from_api(args[:address])
  end

  private

    # invoke_location_from_api(address)
    #   @params: address:String take address and send it to api.

    def invoke_location_from_api(address)
      @goolge_place_data = @google_place_client.spots_by_query(address, :types => ['cities'])[0]
      if @goolge_place_data
        set_location_data(format_address)
      else
        raise ArgumentError, "Can not find location on google place"
      end
    end

    def format_address
      @goolge_place_data.formatted_address.split(",").map{|l| l.gsub(/^\s/, "")}
    end

    def set_location_data(format_address)
      @city = format_address[0]
      @province = format_address[-2]
      @country = format_address[-1]
      @latitude = @goolge_place_data.lat
      @longitude = @goolge_place_data.lng
      @google_place_uid = @goolge_place_data.place_id
    end
end