module Sessionable
  extend ActiveSupport::Concern
  included do
    def generate_token
      SecureRandom.urlsafe_base64(nil, false)
    end
  end
end
