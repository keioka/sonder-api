module Uniqable
  extend ActiveSupport::Concern
  included do
    before_create :generate_uid
  end
 
  private
  
  def generate_uid 
    self.uid = SecureRandom.urlsafe_base64(24, false)
  end

end
