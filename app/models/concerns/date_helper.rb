module DateHelper
  extend ActiveSupport::Concern
 
  def parseFbDateFormat(date)
    return date if date.is_a? Date
    date_array = date.split("/")
    year = date_array.pop
    date = date_array.unshift(year).join("-")
    Date.parse(date)
  end

  def date_parse(string)
    return Date.parse(string)
  end

end
