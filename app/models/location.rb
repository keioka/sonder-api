class Location < ActiveRecord::Base
  
  has_many :users
  has_many :posts
  #validates :google_place_uid, uniqueness: true

  def self.find_or_create_by_api(location_params)
    if !location_params || location_params.empty?
      location = self.first
      return location
    end

    if location_params.is_a?(String)
      send(:find_or_create_by_string, location_params)
    elsif location_params.key?("google_place_uid")
      send(:find_or_create_by_google_place, location_params)
    end 
  end

  private 

  def self.find_or_create_by_google_place(params)
    location = Location.find_by_google_place_params(params)
    if !location
      location = Location.create(params)
    end
    location
  end

  def self.find_by_google_place_params(params)
    place_id = params.try("google_place_uid")
    self.find_by(google_place_uid: place_id) if place_id
  end

  def self.find_or_create_by_string(address)
    google_place_params = GooglePlaceParams.new(address: address)
    place_uid = google_place_params.google_place_uid
    location = self.find_by(google_place_uid: place_uid)
    if !location
      location = self.create(google_place_params.get)
    end
    location
  end
end
