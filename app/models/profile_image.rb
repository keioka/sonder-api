class ProfileImage < ActiveRecord::Base
  belongs_to :profile
  belongs_to :image
  validates_uniqueness_of :profile_id, scope: :image_id
end
