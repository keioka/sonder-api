class ProfileLanguage < ActiveRecord::Base
  belongs_to :profile
  belongs_to :language
  validates_uniqueness_of :profile_id, scope: :language_id
end
