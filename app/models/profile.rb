class Profile < ActiveRecord::Base
  extend DateHelper

  belongs_to :user
  has_many :profile_languages
  has_many :languages, through: :profile_languages
  has_many :profile_interests
  has_many :interests, through: :profile_interests
  has_many :profile_images
  has_many :images, through: :profile_images

  default_scope { includes([:languages, :images, :interests]) }

  def self.create_with_options(profile_params, access_token)
    profile = self.create(profile_params)
    ProfileGenerator.new(profile, profile_params, access_token).init_sub_profile
  end

  def update_with_options(profile_params)
    ProfileGenerator.new(self, profile_params).update_sub_profile
  end
end
