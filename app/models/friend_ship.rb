class FriendShip < ActiveRecord::Base
  belongs_to :user, foreign_key: "user_id", class_name: "User", inverse_of: :friends
  belongs_to :friend, foreign_key: "friend_id", class_name: "User", inverse_of: :reverse_friends
  validates_uniqueness_of :user_id, :scope => :frined_id
end
