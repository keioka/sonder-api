class ProfileInterest < ActiveRecord::Base
  belongs_to :profile
  belongs_to :interest
  validates :interest_id, uniqueness: { scope: :profile_id }
end
