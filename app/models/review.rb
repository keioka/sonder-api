class Review < ActiveRecord::Base
  belongs_to :reviewee, class_name: User, foreign_key: "reviewee_id"
  belongs_to :reviewer, class_name: User, foreign_key: "reviewer_id"

  validates :reviewee_id, :reviewer_id, :content, :rating, presence: true
  validates :rating, :inclusion => { :in => 1..5, :message => "The rating must be between 1 and 5" }
end
