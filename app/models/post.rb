class Post < ActiveRecord::Base
  include Uniqable
  
  has_many :requests
  belongs_to :user
  belongs_to :location

  def self.find_by_location_id(id)
    posts = self.where(location_id: id)
  end

end
