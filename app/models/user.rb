class User < ActiveRecord::Base
  include Uniqable

  belongs_to :session
  belongs_to :location
  has_one :profile
  has_many :posts
  has_many :friend_ships, -> { where approved: true }, class_name: "FriendShip", foreign_key: "user_id", dependent: :destroy
  has_many :reverse_friend_ships, -> { where approved: true } , class_name:  "FriendShip", foreign_key: "friend_id", dependent: :destroy
  has_many :friend_request, -> { where approved: false }, class_name: "FriendShip", foreign_key: "friend_id", dependent: :destroy
  has_many :pending_friend_ships, -> { where approved: false }, class_name: "FriendShip", foreign_key: "user_id", dependent: :destroy
  has_many :friends, :through => :friend_ships, source: :friend
  has_many :reverse_friends, :through => :reverse_friend_ships, source: :user
  has_many :friend_requested_users, through: :friend_request, source: :user  
  has_many :pending_friends, :through => :pending_friend_ships, source: :friend
  has_many :reviews, class_name: "Review", foreign_key: "reviewee_id", dependent: :destroy
  has_many :reviewings, class_name: "Review", foreign_key: "reviewer_id", dependent: :destroy
  
  before_create :generate_uid

  validates :session_id, :location_id, :uid, presence: true
  validates :uid, uniqueness: true

  default_scope { includes([:profile, :location]) }
  delegate :facebook_uid, to: :session

  def self.find_by_city_id(location_id)
    self.where(location_id: location_id)
  end

  def all_friends
    result = []
    result << self.friends
    result << self.reverse_friends
    result.flatten
  end

  def requested_friend
  end

  def update_user(params)
    location_params = params[:location]
    location = Location.find_or_create_by_api(location_params)
    if location
      self.location = location
      self.save
    end
    self.profile.update_with_options(params)
  end

  private
    def generate_uid
      self.uid = self.uid || SecureRandom.urlsafe_base64(nil, false)
    end
end
