class Session < ActiveRecord::Base

  extend DateHelper
  include Sessionable

  has_one :user

  before_create :generate_session_token

  default_scope { includes([:user]) }

  def self.create_or_find_by_facebook_uid(params)
    uid = params[:userID]
    session = self.find_by(facebook_uid: uid)
    if !session
      signup = Signup.new(params)
      session = signup.save
    else
      session.update_token
    end
    session
  end

  def update_token
    self.update_attributes(session_token: self.generate_token)
  end

  def generate_session_token
    self.session_token = generate_token
  end

end
